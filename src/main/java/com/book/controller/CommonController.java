package com.book.controller;


import com.book.model.entity.UploadFile;
import com.github.binarywang.utils.qrcode.MatrixToImageWriter;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.book.model.repository.UploadFileRepository;
import com.book.utils.utils.FileMd5Utils;
import com.book.utils.utils.ImageUploadUtil;
import com.book.utils.utils.LogUtil;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Created by Administrator on 2018/4/2.
 */
@Api( value = "公用接口", description = "公用的接口",tags="common")
@RestController("common")
@RequestMapping("/api/common")
@CrossOrigin
public class CommonController extends BaseController {

    @Value("${site.upload_path}")
    private  String upload_path;

    @Autowired
    UploadFileRepository uploadFileRepository;


    /**
     * 获得二维码
     * @param request
     * @param response
     */
    @RequestMapping(value = "/qrCode",method={RequestMethod.POST,RequestMethod.GET})
    public void getQrCode(HttpServletRequest request, HttpServletResponse response,
                          @RequestParam("content")String content,
                          @RequestParam(value = "width",defaultValue = "200")int width,
                          @RequestParam(value = "height",defaultValue = "200")int height){


        try {
            createdQrCode(URLDecoder.decode(content,"UTF-8"), response.getOutputStream(), width, height);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    /**
     *  生成web版本二维码
     * @param width 二维码宽度
     * @param height 二维码高度
     * @throws IOException
     */
    private void createdQrCode(String content, ServletOutputStream stream, int width, int height) throws IOException {
        if (content != null && !"".equals(content)) {
            try {
                QRCodeWriter writer = new QRCodeWriter();
                BitMatrix m = writer.encode(content, BarcodeFormat.QR_CODE, height, width);
                MatrixToImageWriter.writeToStream(m, "png", stream);
            } catch (WriterException e) {
                e.printStackTrace();
                LogUtil.infoLog.error("生成二维码失败!");
            } finally {
                if (stream != null) {
                    stream.flush();
                    stream.close();
                }
            }
        }
    }

    @RequestMapping(value = "/imageUpload",method={RequestMethod.POST})
    public void imageUpload(HttpServletRequest request, HttpServletResponse response,@RequestParam(value = "key",defaultValue = "") String key) {
        //String DirectoryName = "D:/test/";
        try {

            String fileName = ImageUploadUtil.ckeditor(request, response, upload_path);

            //String imageContextPath =  "/common/download/pic?fileName="+ URLEncoder.encode(upload_path + "/" + fileName,"utf-8");

            File file = new File(upload_path + "/" + fileName);
            String md5 = FileMd5Utils.getFileMD5String(file);
            UploadFile uploadFile = uploadFileRepository.findOneByMd5(md5);
            String ext = fileName.substring(fileName.lastIndexOf(".")).toLowerCase();
            if(uploadFile == null) {
                uploadFile = new UploadFile();
                uploadFile.setName(fileName);
                uploadFile.setExt(ext);
                uploadFile.setMd5(md5);
                uploadFile.setPointCount(1);
                uploadFile.setSize(file.length());

                String imageContextPath = null;
                if (ImageUploadUtil.imgFileTypes.contains(ext)){
                    imageContextPath =  "/common/download/pic/"+ URLEncoder.encode(fileName,"utf-8");
                    uploadFile.setThumbnail(imageContextPath);
                    uploadFile.setType("img");
                }else if(ImageUploadUtil.officeFileTypes.contains(ext)){
                    // 文件的缩略图
                    imageContextPath =  "/common/download/file/"+ URLEncoder.encode(fileName,"utf-8");
                    uploadFile.setThumbnail("/common/download/file");
                    uploadFile.setType("office");
                }else  if (ImageUploadUtil.fileTypes.contains(ext)){
                    imageContextPath =  "/common/download/file/"+ URLEncoder.encode(fileName,"utf-8");
                    uploadFile.setThumbnail("/common/download/file");
                    uploadFile.setType("file");
                }
                uploadFile.setUrl(imageContextPath);
            }else{
                file.delete();//删除这个文件，使用旧文件
                uploadFile.setPointCount(uploadFile.getPointCount()+1);

            }
            uploadFileRepository.save(uploadFile);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("errno",0);

            JSONArray fileNamesJson = new JSONArray();
            fileNamesJson.put(upload_path + "/" + fileName);
            jsonObject.put("paths",fileNamesJson);

            JSONArray jsonArray = new JSONArray();
            jsonArray.put(uploadFile.getUrl());
            jsonObject.put("data",jsonArray);
            JSONArray idsArray = new JSONArray();
            idsArray.put(uploadFile.getId());
            jsonObject.put("ids",idsArray);
            JSONArray md5Array = new JSONArray();
            md5Array.put(uploadFile.getMd5());
            jsonObject.put("md5",md5Array);

            JSONArray extsArray = new JSONArray();
            extsArray.put(uploadFile.getExt());
            jsonObject.put("exts",extsArray);
            JSONArray thumbnailArray = new JSONArray();
            thumbnailArray.put(uploadFile.getThumbnail());
            jsonObject.put("thumbnails",thumbnailArray);
            jsonObject.put("key",key);
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            out.println(jsonObject.toString());
            out.flush();
            out.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    /**
     * 通过url请求返回文件下载
     */
    @RequestMapping(value ="/download/file/{fileName}",method={RequestMethod.GET})
    public void getPic(@PathVariable("fileName") String fileName) throws IOException {

        if(StringUtils.isEmpty(fileName)) {
            fileName = "";
            return;
        }
        fileName = URLDecoder.decode(fileName,"utf-8");
//        String fileName = request.getSession().getServletContext().getRealPath("/")
//                + "resource\\is\\auth\\"
//                + pic.toUpperCase().trim() + ".png";
        File file = new File(upload_path + "/"+fileName);

        //判断文件是否存在如果不存在就返回默认图标
        if(!(file.exists() && file.canRead())) {
            file = new File(request.getSession().getServletContext().getRealPath("/")
                    + "resource/icons/auth/root.png");
        }

        FileInputStream inputStream = new FileInputStream(file);
        byte[] data = new byte[(int)file.length()];
        int length = inputStream.read(data);
        inputStream.close();

        //response.setContentType("image/png");
        response.setContentType("application/binary;charset=UTF-8");
        response.setHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode(fileName, "UTF-8"));
        response.setContentLength(data.length);


        OutputStream stream = response.getOutputStream();
        stream.write(data);
        stream.flush();
        stream.close();
    }
    /**
     * 通过url请求返回图像的字节流
     */
    @RequestMapping(value ="/download/pic/{fileName}",method={RequestMethod.GET})
    public void getPicPath(@PathVariable("fileName") String fileName,
                       HttpServletRequest request,
                       HttpServletResponse response) throws IOException {

        if(StringUtils.isEmpty(fileName)) {
            fileName = "";
            return;
        }
        fileName = URLDecoder.decode(fileName,"utf-8");
//        String fileName = request.getSession().getServletContext().getRealPath("/")
//                + "resource\\is\\auth\\"
//                + pic.toUpperCase().trim() + ".png";
        File file = new File(upload_path+"/"+fileName);

        //判断文件是否存在如果不存在就返回默认图标
        if(!(file.exists() && file.canRead())) {
            file = new File(request.getSession().getServletContext().getRealPath("/")
                    + "resource/icons/auth/root.png");
        }

        FileInputStream inputStream = new FileInputStream(file);
        byte[] data = new byte[(int)file.length()];
        int length = inputStream.read(data);
        inputStream.close();

        response.setContentType("image/png");

        OutputStream stream = response.getOutputStream();
        stream.write(data);
        stream.flush();
        stream.close();
    }
    /**
     * 通过url请求返回图像的字节流
     */
    @RequestMapping(value ="/download/logo",method={RequestMethod.GET})
    public void logo(HttpServletRequest request,
                       HttpServletResponse response) throws IOException {

        File file = ResourceUtils.getFile("classpath:static/images/logo.jpg");

        //判断文件是否存在如果不存在就返回默认图标
        if(!(file.exists() && file.canRead())) {
            file = new File(request.getSession().getServletContext().getRealPath("/")
                    + "resource/icons/auth/root.png");
        }

        FileInputStream inputStream = new FileInputStream(file);
        byte[] data = new byte[(int)file.length()];
        int length = inputStream.read(data);
        inputStream.close();

        response.setContentType("image/jpg");

        OutputStream stream = response.getOutputStream();
        stream.write(data);
        stream.flush();
        stream.close();
    }
    /**
     * 通过url请求返回图像的字节流
     */
    @RequestMapping(value ="/download/file",method={RequestMethod.GET})
    public void fileicon(HttpServletRequest request,
                     HttpServletResponse response) throws IOException {

        File file = ResourceUtils.getFile("classpath:static/images/file.png");

        //判断文件是否存在如果不存在就返回默认图标
        if(!(file.exists() && file.canRead())) {
            file = new File(request.getSession().getServletContext().getRealPath("/")
                    + "resource/icons/auth/root.png");
        }

        FileInputStream inputStream = new FileInputStream(file);
        byte[] data = new byte[(int)file.length()];
        int length = inputStream.read(data);
        inputStream.close();

        response.setContentType("image/jpg");

        OutputStream stream = response.getOutputStream();
        stream.write(data);
        stream.flush();
        stream.close();
    }








}
