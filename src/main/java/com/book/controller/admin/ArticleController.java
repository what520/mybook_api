package com.book.controller.admin;


import com.book.controller.BaseController;
import com.book.model.MsgVo;
import com.book.model.criteria.Criteria;
import com.book.model.criteria.Restrictions;
import com.book.model.entity.Article;
import com.book.model.entity.ArticleGroup;
import com.book.model.entity.UploadFile;
import com.book.model.repository.ArticleGroupRepository;
import com.book.model.repository.ArticleRepository;
import com.book.model.repository.UploadFileRepository;
import com.book.model.repository.UserRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Administrator on 2018/4/2.
 */
@Api( value = "文章管理", description = "文章的接口",tags="admin-article")
@RestController("admin_article_controller")
@RequestMapping("/api/admin/article")
@CrossOrigin
public class ArticleController extends BaseController {
    @Autowired
    ArticleGroupRepository articleGroupRepository;
    @Autowired
    ArticleRepository articleRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    UploadFileRepository uploadFileRepository;



    @ApiOperation(value="列表", notes="列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "pageSize", value = "每页行数,默认10条", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "",method = RequestMethod.GET)
    @ResponseBody
    public MsgVo list(

            @RequestParam(value = "id",defaultValue = "0")Long id,
            @RequestParam(value = "page",defaultValue = "1")Integer page,
            @RequestParam(value = "pageSize",defaultValue = "10")Integer pageSize){

        MsgVo msgVo =new MsgVo();

        Criteria<Article> criteria = new Criteria<>();
        if(id > 0){
            criteria.add(Restrictions.eq("id",id));
        }
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable =  new PageRequest(page-1, pageSize, sort);
        Page<Article> articles = articleRepository.findAll(criteria,pageable);

        msgVo.getData().put("articles",articles);
        msgVo.setMsg("获取成功");
        return msgVo;
    }

    @ApiOperation(value="创建/修改", notes="创建/修改")
    @ApiImplicitParams({
    })
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public MsgVo add(@RequestParam(value = "id",defaultValue = "0")Long id,
                     @RequestParam("title")String title,
                     @RequestParam(value = "author",defaultValue = "无",required = false)String author,
                     @RequestParam(value = "language",defaultValue = "zh_cn")String language,
                     @RequestParam(value = "press",defaultValue = "无",required = false)String press,
                     @RequestParam(value = "publicationTime",defaultValue = "",required = false)String publicationTime,
                     @RequestParam(value = "des",defaultValue = "",required = false)String des,
                     @RequestParam("group_id")Long group_id,
                     @RequestParam(value = "photo_id",defaultValue = "0")Long photo_id

    ){
        MsgVo msgVo = new MsgVo();
        Article article = null;
        if(id > 0){
            article = articleRepository.findById(id).orElse(null);
            if(article == null){
                msgVo.setCode(40002);
                msgVo.setMsg("文章不存在");
                return msgVo;
            }
        }else{
            article = new Article();
        }
        article.setTitle(title);
        article.setAuthor(author);
        article.setLanguage(language);
        article.setPress(press);

        if(!publicationTime.equals("")) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            try {
                article.setPublicationTime(simpleDateFormat.parse(publicationTime));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        article.setDes(des);

        if(photo_id > 0) {
            UploadFile uploadFile = uploadFileRepository.findById(photo_id).orElse(null);
            article.setPhoto(uploadFile);
        }

        ArticleGroup articleGroup = articleGroupRepository.findById(group_id).orElse(null);
        article.setGroup(articleGroup);

        articleRepository.save(article);
        return msgVo;
    }

    @ApiOperation(value="删除", notes="删除")
    @ApiImplicitParams({
    })
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public MsgVo delete(@PathVariable("id")Long id){
        MsgVo msgVo = new MsgVo();

        Article article = articleRepository.findById(id).orElse(null);
        if(article == null){
            msgVo.setCode(40001);
            msgVo.setMsg("文章不存在");
            return msgVo;
        }
        //TODO 判断是否有文章章节
        articleRepository.delete(article);
        return msgVo;
    }

}
