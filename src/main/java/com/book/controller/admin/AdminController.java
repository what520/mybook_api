package com.book.controller.admin;


import com.book.controller.BaseController;
import com.book.model.MsgVo;
import com.book.model.criteria.Criteria;
import com.book.model.criteria.Restrictions;
import com.book.model.entity.Admin;
import com.book.model.entity.User;
import com.book.model.repository.AdminRepository;
import com.book.model.repository.UserRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Administrator on 2018/4/2.
 */
@Api( value = "管理员", description = "后台管理员的接口",tags="admin-admin")
@RestController("admin_controller")
@RequestMapping("/api/admin")
@CrossOrigin
public class AdminController extends BaseController {
    @Autowired
    AdminRepository adminRepository;
    @Autowired
    UserRepository userRepository;




    /**
     * 登录
     * @return
     */
    @ApiOperation(value="后台管理员登录授权", notes="根据用户名和密码来获取管理员详细信息及登录token")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "account", value = "账号", required = true, dataType = "String",paramType="form"),
            @ApiImplicitParam(name = "password", value = "密码", required = true, dataType = "String",paramType="form"),
            @ApiImplicitParam(name = "holddays", value = "保持登录状态天数(默认2小时)", required = false, dataType = "int",paramType="form")
    })
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    @ResponseBody
    public MsgVo login(@RequestParam("account")String account,
                       @RequestParam("password")String password,
                       @RequestParam(value = "holddays",defaultValue = "0")int holddays
    ){
        MsgVo msgVo = new MsgVo();
        Criteria<User> criteria = new Criteria<>();
        criteria.add(Restrictions.eq("account",account));

        criteria.add(Restrictions.eq("isDelete",false));
        User user = userRepository.findOne(criteria).orElse(null);
        if(user == null){
            msgVo.setCode(40001);
            msgVo.setMsg("帐号或密码错误");
            return msgVo;
        }
        if(!user.getPwd(password).equals(user.getPassword())){
            msgVo.setCode(40002);
            msgVo.setMsg("帐号或密码错误");
            return msgVo;
        }

        Criteria<Admin> adminCriteria = new Criteria<>();
        adminCriteria.add(Restrictions.eq("user",user));
        Admin admin = adminRepository.findOne(adminCriteria).orElse(null);
        if(admin == null){
            msgVo.setCode(40003);
            msgVo.setMsg("帐号或密码错误");
            return msgVo;
        }
        try {
            removeToken(user);
            saveToken("admin",user,holddays);
            msgVo.getData().put("user",admin);
        }catch (Exception e) {
            e.printStackTrace();
            msgVo.setCode(40004);
            msgVo.setMsg("帐号或密码错误");
            return msgVo;
        }
        return msgVo;
    }



    @ApiOperation(value="后台管理员退出登录授权", notes="后台管理员退出登录授权接口")
    @ApiImplicitParams({
    })
    @RequestMapping(value = "/logout",method = RequestMethod.POST)
    @ResponseBody
    public MsgVo logout(){
        MsgVo msgVo = new MsgVo();
        Admin admin = (Admin) request.getAttribute("user");
        if(admin != null) {
            removeToken(admin.getUser());
        }
        return msgVo;
    }




    @ApiOperation(value="修改管理员密码", notes="修改管理员密码")
    @ApiImplicitParams({
    })
    @RequestMapping(value = "/setpassword",method = RequestMethod.PUT)
    @ResponseBody
    public MsgVo setpassword(@RequestParam("newpwd")String password, @RequestParam(value = "oldpwd")String oldPassword){
        MsgVo msgVo = new MsgVo();
        Admin admin = (Admin) request.getAttribute("user");
        User user = admin.getUser();

        if(!user.getPwd(oldPassword).equals(user.getPassword())){
            msgVo.setCode(40003);
            msgVo.setMsg("旧密码错误");
            return msgVo;
        }
        user.setPassword(user.getPwd(password));
        removeToken(user);
        userRepository.save(user);
        return msgVo;
    }

}
