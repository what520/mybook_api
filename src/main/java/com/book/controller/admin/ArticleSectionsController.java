package com.book.controller.admin;


import com.book.controller.BaseController;
import com.book.model.MsgVo;
import com.book.model.criteria.Criteria;
import com.book.model.criteria.Restrictions;
import com.book.model.entity.Article;
import com.book.model.entity.ArticleSections;
import com.book.model.entity.UploadFile;
import com.book.model.repository.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Administrator on 2018/4/2.
 */
@Api( value = "文章章节管理", description = "文章章节的接口",tags="admin-article-sections")
@RestController("admin_article_sections_controller")
@RequestMapping("/api/admin/article/sections")
@CrossOrigin
public class ArticleSectionsController extends BaseController {
    @Autowired
    ArticleSectionsRepository articleSectionsRepository;
    @Autowired
    ArticleRepository articleRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    UploadFileRepository uploadFileRepository;



    @ApiOperation(value="列表", notes="列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "pageSize", value = "每页行数,默认10条", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "",method = RequestMethod.GET)
    @ResponseBody
    public MsgVo list(
            @RequestParam(value = "id",defaultValue = "0")Long id,
            @RequestParam(value = "article_id",defaultValue = "0")Long article_id,
            @RequestParam(value = "page",defaultValue = "1")Integer page,
            @RequestParam(value = "pageSize",defaultValue = "10")Integer pageSize){

        MsgVo msgVo =new MsgVo();

        Criteria<ArticleSections> criteria = new Criteria<>();
        if(id > 0){
            criteria.add(Restrictions.eq("id",id));
        }
        if(article_id > 0){
            Article article = articleRepository.findById(article_id).orElse(null);
            if(article == null){
                msgVo.getData().put("articleSections",null);
                msgVo.setMsg("没有对应的文章");
                return msgVo;
            }
            criteria.add(Restrictions.eq("article",article));
        }


        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable =  new PageRequest(page-1, pageSize, sort);
        Page<ArticleSections> articleSections = articleSectionsRepository.findAll(criteria,pageable);
        List<ArticleSections> articleSectionsList = articleSections.getContent();
        for(ArticleSections articleSections1:articleSectionsList){
            articleSections1.setContent(org.apache.commons.text.StringEscapeUtils.unescapeHtml4(articleSections1.getContent()));
        }
        msgVo.getData().put("articleSections",articleSections);
        msgVo.setMsg("获取成功");
        return msgVo;
    }

    @ApiOperation(value="创建/修改", notes="创建/修改")
    @ApiImplicitParams({
    })
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public MsgVo add(@RequestParam(value = "id",defaultValue = "0")Long id,
                     @RequestParam("title")String title,
                     @RequestParam("sTitle")String sTitle,
                     @RequestParam(value = "type",defaultValue = "1")int type,
                     @RequestParam(value = "content",defaultValue = "")String content,
                     @RequestParam("file_id")Long file_id,
                     @RequestParam("sort")Integer sort,
                     @RequestParam("price")Integer price,
                     @RequestParam(value = "filePreViewPages",defaultValue = "0")int filePreViewPages,
                     @RequestParam("article_id")Long article_id


    ){
        MsgVo msgVo = new MsgVo();
        ArticleSections articleSections = null;
        if(id > 0){
            articleSections = articleSectionsRepository.findById(id).orElse(null);
            if(articleSections == null){
                msgVo.setCode(40002);
                msgVo.setMsg("章节不存在");
                return msgVo;
            }
        }else{
            articleSections = new ArticleSections();
        }
        articleSections.setTitle(title);
        articleSections.setSTitle(sTitle);
        articleSections.setType(type);
        articleSections.setPrice(price);
        articleSections.setFilePreViewPages(filePreViewPages);
        articleSections.setSort(sort);
        if(type == 2 && file_id > 0) {
            UploadFile uploadFile = uploadFileRepository.findById(file_id).orElse(null);
            articleSections.setFile(uploadFile);
            articleSections.setContent("");
        }else if(type == 1){
            content = org.apache.commons.text.StringEscapeUtils.escapeHtml4(content);
            articleSections.setContent(content);
            articleSections.setFile(null);
        }
        Article article = articleRepository.findById(article_id).orElse(null);
        articleSections.setArticle(article);

        articleSectionsRepository.save(articleSections);
        return msgVo;
    }

    @ApiOperation(value="删除", notes="删除")
    @ApiImplicitParams({
    })
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public MsgVo delete(@PathVariable("id")Long id){
        MsgVo msgVo = new MsgVo();

        ArticleSections articleSections = articleSectionsRepository.findById(id).orElse(null);
        if(articleSections == null){
            msgVo.setCode(40001);
            msgVo.setMsg("章节不存在");
            return msgVo;
        }
        articleSectionsRepository.delete(articleSections);
        return msgVo;
    }

}
