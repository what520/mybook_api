package com.book.controller.admin;


import com.book.controller.BaseController;
import com.book.model.MsgVo;
import com.book.model.criteria.Criteria;
import com.book.model.criteria.Restrictions;
import com.book.model.entity.Article;
import com.book.model.entity.OauthUrl;
import com.book.model.repository.*;
import com.book.utils.utils.MD5Util;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/4/2.
 */
@Api( value = "授权地址管理", description = "授权地址的接口",tags="admin-oauth-urls")
@RestController("admin_oauth_urls_controller")
@RequestMapping("/api/admin/oauth/urls")
@CrossOrigin
public class OauthUrlController extends BaseController {

    @Autowired
    ArticleRepository articleRepository;

    @Autowired
    OauthUrlRepository oauthUrlRepository;


    @ApiOperation(value="列表", notes="列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "pageSize", value = "每页行数,默认10条", required = false, dataType = "int", paramType = "query"),
    })
    @RequestMapping(value = "",method = RequestMethod.GET)
    @ResponseBody
    public MsgVo list(

            @RequestParam(value = "id",defaultValue = "0")Long id,
            @RequestParam(value = "page",defaultValue = "1")Integer page,
            @RequestParam(value = "pageSize",defaultValue = "10")Integer pageSize){

        MsgVo msgVo =new MsgVo();

        Criteria<OauthUrl> criteria = new Criteria<>();
        if(id > 0){
            criteria.add(Restrictions.eq("id",id));
        }
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable =  new PageRequest(page-1, pageSize, sort);
        Page<OauthUrl> oauthUrls = oauthUrlRepository.findAll(criteria,pageable);

        msgVo.getData().put("oauthUrls",oauthUrls);
        msgVo.setMsg("获取成功");
        return msgVo;
    }

    @ApiOperation(value="创建", notes="创建")
    @ApiImplicitParams({
    })
    @RequestMapping(value = "",method = RequestMethod.POST)
    @ResponseBody
    public MsgVo add(@RequestParam(value = "article_id",defaultValue = "0")Long article_id,
                     @RequestParam(value = "count",defaultValue = "1")Integer count

    ){
        MsgVo msgVo = new MsgVo();
        Article article = articleRepository.findById(article_id).orElse(null);
        if(article == null){
            msgVo.setCode(40002);
            msgVo.setMsg("文章不存在");
            return msgVo;
        }
        long time =  System.currentTimeMillis();
        List<OauthUrl> oauthUrlList = new ArrayList<>();
        for(int i = 0 ; i < count ; i ++){
            OauthUrl oauthUrl = new OauthUrl();
            oauthUrl.setArticle(article);
            try {
                oauthUrl.setUuid(MD5Util.MD5Encode(""+i+time));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            oauthUrlList.add(oauthUrl);
        }

        oauthUrlRepository.saveAll(oauthUrlList);
        return msgVo;
    }

    @ApiOperation(value="删除", notes="删除")
    @ApiImplicitParams({
    })
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public MsgVo delete(@PathVariable("id")Long id){
        MsgVo msgVo = new MsgVo();

        OauthUrl oauthUrl = oauthUrlRepository.findById(id).orElse(null);
        if(oauthUrl == null){
            msgVo.setCode(40001);
            msgVo.setMsg("链接不存在");
            return msgVo;
        }
        oauthUrlRepository.delete(oauthUrl);
        return msgVo;
    }

}
