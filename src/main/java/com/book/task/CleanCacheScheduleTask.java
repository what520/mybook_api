package com.book.task;

import com.book.model.service.TmpCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
//https://www.cnblogs.com/mmzs/p/10161936.html
//https://www.cnblogs.com/sunjie9606/archive/2012/03/15/2397626.html

/**
 * 清理过期缓存定时器
 */
@Component
@EnableScheduling   // 1.开启定时任务
@EnableAsync        // 2.开启多线程
public class CleanCacheScheduleTask {

    @Autowired
    TmpCacheService tmpCacheService;

    /**
     * 清理过期缓存定时器
     * @throws InterruptedException
     */
    @Async
    @Scheduled(fixedDelay = 1000*60*60*24)  //间隔24小时清理一次
    public void cleanCacheTask() throws InterruptedException {
        //System.out.println("第1个定时任务开始 : " + LocalDateTime.now().toLocalTime() + "\r\n线程 : " + Thread.currentThread().getName());
        tmpCacheService.clearExpireOut();
    }
}
