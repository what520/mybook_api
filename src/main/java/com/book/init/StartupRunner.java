package com.book.init;

/**
 * Created by Administrator on 2017/11/8.
 */

import com.book.model.config.Config;
import com.book.model.entity.*;
import com.book.model.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * 服务启动执行
 *
 */
@Component
@Order(value=1)
public class StartupRunner implements CommandLineRunner {
    @Autowired
    AdminRepository adminRepository;

    @Autowired
    UserRepository userRepository;

    @Value("${site.host}")
    private  String host;

    @Value("${site.file.host}")
    private  String filehost;


    @Override
    public void run(String... args) throws Exception {
        addadmin();
        Config.host = host;
        Config.fileHost = filehost;
    }


    //初始化超管
    private void addadmin(){
        //增加超管
        List<Admin> admins = adminRepository.findAll();
        if(admins == null || admins.size() == 0){
            User user = new User();
            user.setAccount("admin");
            user.setPassword(user.getPwd("888888"));
            user = userRepository.saveAndFlush(user);

            Admin admin = new Admin();
            admin.setName("平台管理员");
            admin.setUser(user);
            adminRepository.saveAndFlush(admin);
        }
    }
    //初始化参数




}