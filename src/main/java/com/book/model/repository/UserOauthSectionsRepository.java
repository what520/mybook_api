package com.book.model.repository;


import com.book.model.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserOauthSectionsRepository extends JpaSpecificationExecutor<UserOauthSections>,JpaRepository<UserOauthSections, Long>,PagingAndSortingRepository<UserOauthSections, Long> {
    UserOauthSections findOneByUserAndArticleSections(User user, ArticleSections articleSections);
    List<UserOauthSections> findAllByUserAndArticle(User user, Article article);
}