package com.book.model.repository;


import com.book.model.entity.Article;
import com.book.model.entity.OauthUrl;
import com.book.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface OauthUrlRepository extends JpaSpecificationExecutor<OauthUrl>,JpaRepository<OauthUrl, Long>,PagingAndSortingRepository<OauthUrl, Long> {
    OauthUrl findOneByUuid(String uuid);
    OauthUrl findOneByUserAndArticle(User user, Article article);
}