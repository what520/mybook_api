package com.book.model.repository;


import com.book.model.entity.Article;
import com.book.model.entity.ArticleSections;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 平台超管
 * Created by Administrator on 2018/4/2.
 */

@Repository
public interface ArticleSectionsRepository extends JpaSpecificationExecutor<ArticleSections>,JpaRepository<ArticleSections, Long>,PagingAndSortingRepository<ArticleSections, Long> {
    List<ArticleSections> findAllByArticle(Article article);
}