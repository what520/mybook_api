package com.book.model.repository;


import com.book.model.entity.OauthUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface OauthUserRepository extends JpaSpecificationExecutor<OauthUser>,JpaRepository<OauthUser, Long>,PagingAndSortingRepository<OauthUser, Long> {
    OauthUser findOneByOpenid(String openid);

}