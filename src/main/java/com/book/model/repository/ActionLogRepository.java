package com.book.model.repository;


import com.book.model.entity.ActionLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ActionLogRepository extends JpaSpecificationExecutor<ActionLog>,JpaRepository<ActionLog, Long>,PagingAndSortingRepository<ActionLog, Long> {

}