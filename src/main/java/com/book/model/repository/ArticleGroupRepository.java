package com.book.model.repository;


import com.book.model.entity.ArticleGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * 平台超管
 * Created by Administrator on 2018/4/2.
 */

@Repository
public interface ArticleGroupRepository extends JpaSpecificationExecutor<ArticleGroup>,JpaRepository<ArticleGroup, Long>,PagingAndSortingRepository<ArticleGroup, Long> {

}