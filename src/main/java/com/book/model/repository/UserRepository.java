package com.book.model.repository;


import com.book.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2018/4/2.
 */

@Repository
public interface UserRepository extends JpaSpecificationExecutor<User>,JpaRepository<User, Long>,PagingAndSortingRepository<User, Long> {
    User findOneByAccountAndIsDelete(String account,boolean isDelete);

}