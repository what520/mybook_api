package com.book.model.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.List;

/**
 * 分章分组
 */

@Data
@Entity
public class ArticleGroup extends BaseEntity{
    private String name;
    private Long parentID;
    @Transient
    private List<ArticleGroup> children;
}
