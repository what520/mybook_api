package com.book.model.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * 分章章节
 */

@Data
@Entity
public class ArticleSections extends BaseEntity{
    private Integer sort;
    private String sTitle;//第几章之类的名字
    private String title;//本节名字
    @ManyToOne(targetEntity = Article.class)
    @JoinColumn(name = "article_id",referencedColumnName = "id")
    private Article article;

    @Column(columnDefinition = "text")
    private String content = null;

    private Integer price = 0;//价格

    private int type = 1;//1.富文本，2.pdf

    @ManyToOne(targetEntity = UploadFile.class)
    @JoinColumn(name = "file_id",referencedColumnName = "id")
    private UploadFile file = null;//附件版本
    private int filePreViewPages = 0;//附件可预览页数

    @Transient
    private Integer startPage;


}
