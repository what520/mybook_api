package com.book.model.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * 操作日志
 */


@Entity
@Data
public class ActionLog extends BaseEntity{


    private String obj;//操作对像

    private String type;

    @Column(columnDefinition = "text")
    private String note;

}
