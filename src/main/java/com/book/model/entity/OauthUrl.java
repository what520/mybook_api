package com.book.model.entity;

import com.book.model.config.Config;
import lombok.Data;

import javax.persistence.*;

/**
 * 授权地址
 */

@Data
@Entity
public class OauthUrl extends BaseEntity{

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private User user = null;

    @ManyToOne(targetEntity = Article.class)
    @JoinColumn(name = "article_id",referencedColumnName = "id")
    private Article article;

    private String uuid;

    @Transient
    private String url;

    public String getUrl() {
        url = Config.host+"/api/index/article/"+uuid;
        return url;
    }
}
