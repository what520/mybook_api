package com.book.model.entity;

import com.book.model.config.Config;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

/**
 * 授权地址
 */

@Data
@Entity
public class UserOauthSections extends BaseEntity{

    @ManyToOne(targetEntity = User.class)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private User user = null;

    @ManyToOne(targetEntity = ArticleSections.class)
    @JoinColumn(name = "article_section_id",referencedColumnName = "id")
    private ArticleSections articleSections;

    @ManyToOne(targetEntity = Article.class)
    @JoinColumn(name = "article_id",referencedColumnName = "id")
    private Article article;
}
