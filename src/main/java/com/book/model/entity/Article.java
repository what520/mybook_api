package com.book.model.entity;

import com.book.model.config.Config;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * 分章
 */

@Data
@Entity
public class Article extends BaseEntity{
    private String title;
    
    private String author;

    @Column(columnDefinition = "text")
    private String des;//简介
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date publicationTime = null;//出版时间
    private String language;
    private String press;//出版社

    @ManyToOne(targetEntity = UploadFile.class)
    @JoinColumn(name = "photo_id",referencedColumnName = "id")
    private UploadFile photo;//封面图

    @ManyToOne(targetEntity = ArticleGroup.class)
    @JoinColumn(name = "group_id",referencedColumnName = "id")
    private ArticleGroup group;//


    @Transient
    private String url;

    public String getUrl() {
        url = Config.host+"/api/index/article/"+id+"/preview";
        return url;
    }
}
