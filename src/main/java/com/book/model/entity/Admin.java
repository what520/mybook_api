package com.book.model.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * 超管
 */
@Data
@Entity
public class Admin extends BaseEntity {
    private String name;
    private Integer sex;
    private Integer age;
    private String telephon;//电话
    @OneToOne(targetEntity = User.class)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private User user;

}
