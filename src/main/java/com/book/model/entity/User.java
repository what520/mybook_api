package com.book.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.book.utils.utils.MD5Util;
import lombok.Data;

import javax.persistence.*;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

/**
 * 用户表
 */
@Data
@Entity
public class User extends BaseEntity {

    private String account;

    @JsonIgnore
    private String password;

    private String accessToken;

    @ManyToOne(targetEntity = UploadFile.class)
    @JoinColumn(name = "photo_id",referencedColumnName = "id")
    private UploadFile photo;//头像

    private int isFreeze = 0;//0:正常，1冻结

    @Temporal(value = TemporalType.TIMESTAMP)
    private Date lastLoginTime = null;//最后登录时间

    public String getPwd(String srcpwd){
        try {
            return MD5Util.MD5Encode(srcpwd + account);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
    public boolean checkPwd(String srcpwd){
            String reg = "^(?![A-Za-z0-9]+$)(?![a-z0-9\\\\W]+$)(?![A-Za-z\\\\W]+$)(?![A-Z0-9\\\\W]+$)[a-zA-Z0-9\\\\W]{8,}$";
            return srcpwd.matches(reg);
    }
}
