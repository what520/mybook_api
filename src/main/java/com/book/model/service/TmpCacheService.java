package com.book.model.service;

public interface TmpCacheService {
    /**
     * 获取值
     * @param key
     * @return
     */
    public String get(String key);

    public void set(String key,String value,long expireIn);

    public void delayedExpireTime(String key,long expireIn);

    public void delete(String key);

    public void clearExpireOut();

}
