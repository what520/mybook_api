package com.book.model.service.impl;

import com.book.model.entity.UploadFile;
import com.book.model.repository.UploadFileRepository;
import com.book.model.service.UploadFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service("UploadFileService")
public class UploadFileServiceImpl implements UploadFileService {

    @Autowired
    UploadFileRepository uploadFileRepository;
    @Override
    public void delete(UploadFile uploadFile) {
        if(uploadFile != null) {
            if (uploadFile.getPointCount() <= 1) {
                uploadFileRepository.delete(uploadFile);
            } else {
                uploadFile.setPointCount(uploadFile.getPointCount() - 1);
                uploadFileRepository.save(uploadFile);
            }
        }
    }
}
