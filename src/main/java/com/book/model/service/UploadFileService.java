package com.book.model.service;

import com.book.model.entity.UploadFile;

public interface UploadFileService {

    public void delete(UploadFile uploadFile);

}
