package com.book.interceptor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by Administrator on 2017/11/14.
 */
@Configuration
public class MyWebAppConfigurer extends WebMvcConfigurerAdapter {

    @Bean
    public HandlerInterceptor getAdminAuthInterceptor(){
        return new AdminAuthInterceptor();
    }

    @Value("${spring.profiles.active}")
    private  String active;


    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        // 多个拦截器组成一个拦截器链
        // addPathPatterns 用于添加拦截规则
        // excludePathPatterns 用户排除拦截
        if(active.equals("prod")) {
            registry.addInterceptor(getAdminAuthInterceptor()).addPathPatterns("/api/admin/**").excludePathPatterns("/api/admin/login");
        }
        super.addInterceptors(registry);


    }

}
