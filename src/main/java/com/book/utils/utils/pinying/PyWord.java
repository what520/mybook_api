package com.book.utils.utils.pinying;

import lombok.Data;

@Data
public class PyWord {
    private String type = "zi";
    private String word = "";
    private String pinying = "";
}
