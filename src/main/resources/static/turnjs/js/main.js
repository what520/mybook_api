/**
 * Created by ChengYa on 2016/6/18.
 */

//判断手机类型
window.onload = function () {
    //alert($(window).height());
    var u = navigator.userAgent;
    if (u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {//安卓手机
    } else if (u.indexOf('iPhone') > -1) {//苹果手机
        //屏蔽ios下上下弹性
        $(window).on('scroll.elasticity', function (e) {
            e.preventDefault();
        }).on('touchmove.elasticity', function (e) {
            e.preventDefault();
        });
    } else if (u.indexOf('Windows Phone') > -1) {//winphone手机
    }
    yepnope({
        test: Modernizr.csstransforms,
        yep: ['/turnjs/js/turn.js'],
        complete: loadApp
    });
}
//配置turn.js
function loadApp() {
    $(".flipbook-viewport").show();
    var w = $(window).width();
    var h = $(window).height();
    $('.flipboox').width(w).height(h);
    $('.contentDiv').height(h);
    $('.contentDiv2').height(h);
    //$("#a_photo").width(w).height(h);

    $(window).resize(function () {
        w = $(window).width();
        h = $(window).height();
        $('.flipboox').width(w).height(h);

    });
    $('.flipbook').turn({
        // Width
        width: w,
        // Height
        height: h,
        // Elevation
        elevation: 5,
        display: 'single',
        // Enable gradients
        gradients: true,
        // Auto center this flipbook
        autoCenter: true,
        when: {
            turning: function (e, page, view) {
                if (page == 1) {
                    $(".btnImg").css("display", "none");
                    $(".mark").css("display", "block");
                } else {
                    $(".btnImg").css("display", "block");
                    $(".mark").css("display", "none");
                }
                if (page == 41) {
                    $(".nextPage").css("display", "none");
                } else {
                    $(".nextPage").css("display", "block");
                }
            },
            turned: function (e, page, view) {
                console.log(page);
                var total = $(".flipbook").turn("pages");//总页数
                if (page == 1) {
                    $(".return").css("display", "block");
                    $(".btnImg").css("display", "block");
                } else {
                    $(".return").css("display", "block");
                    $(".btnImg").css("display", "block");
                }
                if (page == 2) {
                    $(".catalog").css("display", "block");
                } else {
                    $(".catalog").css("display", "none");
                }
            }
        }
    });

}




