#### 介绍

电子书系统

业余写的一个小系统，不会持续维护的，有兴趣的可以拿去玩玩。

前端在：https://gitee.com/what520/mybook_admin


#### 软件架构

1.技术框架：
后端：spring boot + jpa  
后台管理前端：vue.js + elementUI

2.技术架构
采用前后端分离，完全使用restful风格编写的接口。


#### 安装教程

1.  安装好java环境并配置
2.  安装好数据库，创建好数据库（无需建表）
3.  修改程序中的application-prod.properties中的数据库连接配置
4.  运行程序即可（jpa会自动建表）